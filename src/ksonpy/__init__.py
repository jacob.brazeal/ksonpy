import os.path
import sys

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

from db import *
from parser import *
from compiler import *
