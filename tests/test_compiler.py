import unittest

import compiler
from ksonpy.compiler import KSONCompiler


class TestCompiler(unittest.TestCase):

    def test_references(self):
        kson = '''{
    "description": "This is a KSON document containing information about global GDP.",
    /* The next line shows a `ref` datatype, which is delimited by the tokens `<<` and `>>`.
       Optionally, as here, it can start with an exclamation point (!), which means that the contents of the file
       itself will not be included in the JSON output.
       Note the alias "as gdp" at the end: this is how we name an object in our file so that it can be referenced
       by a SQL query.
       */
    "source": <<!https://raw.githubusercontent.com/datasets/gdp/master/data/gdp.csv>> as gdp,
    /* A SQL query is delimited by triple backticks (```). We can refer to aliases in our file by prefixing them
        with `$`. This query will be evaluated as a list of dictionaries. */
    "bot5GDPs": ```select * from "$gdp"
                    where year in (select max(year) from "$gdp")
                    order by value desc
                    limit 5;```
}
'''
        json = KSONCompiler().run(kson)
        expected = '''{"description": "This is a KSON document containing information about global GDP.", "source": "https://raw.githubusercontent.com/datasets/gdp/master/data/gdp.csv", "bot5GDPs": [{"CountryName": "World", "CountryCode": "WLD", "Year": 2016, "Value": 75845109381590.5}, {"CountryName": "High income", "CountryCode": "HIC", "Year": 2016, "Value": 48557463061833.1}, {"CountryName": "OECD members", "CountryCode": "OED", "Year": 2016, "Value": 47552622005124.2}, {"CountryName": "Post-demographic dividend", "CountryCode": "PST", "Year": 2016, "Value": 45213309475250.0}, {"CountryName": "IDA & IBRD total", "CountryCode": "IBT", "Year": 2016, "Value": 27972819139754.5}]}'''
        self.assertEqual(json, expected)

    def test_joins(self):
        kson = '''{
    "description": "This is a KSON document which cross-references two data sources to compute per capita GDP.",
    /* Data source 1 is a CSV file */
    "sourceGDP": <<!https://raw.githubusercontent.com/datasets/gdp/master/data/gdp.csv>> as gdp,
    /* Data source 2 is a JSON file. While KSON attempts to infer the data type automatically, you can also specify it
     explicitly with a `|<type>` after the URL as shown here: */
    "sourcePopulation": <<!https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-population.json|json>> as pop,
    "gdpAndPopulation": ```
            select p.country, g.year, g.value gdp, p.population, g.value/p.population perCapita
            from "$gdp" g left join "$pop" p on g.countryName = p.country
             where year in (select max(year) from "$gdp")
             order by g.value/p.population desc
             limit 5;
         ```
}'''
        json = KSONCompiler().run(kson)
        expected = '''{"description": "This is a KSON document which cross-references two data sources to compute per capita GDP.", "sourceGDP": "https://raw.githubusercontent.com/datasets/gdp/master/data/gdp.csv", "sourcePopulation": "https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-population.json", "gdpAndPopulation": [{"country": "Luxembourg", "Year": 2016, "gdp": 58631324559.4484, "population": 607950, "perCapita": 96441.03061016268}, {"country": "Switzerland", "Year": 2016, "gdp": 668851296244.236, "population": 8513227, "perCapita": 78566.12965262597}, {"country": "Norway", "Year": 2016, "gdp": 371076190476.19, "population": 5311916, "perCapita": 69857.31522791211}, {"country": "Ireland", "Year": 2016, "gdp": 304819020500.641, "population": 4867309, "perCapita": 62625.77956333592}, {"country": "United States", "Year": 2016, "gdp": 18624475000000.0, "population": 326687501, "perCapita": 57010.06295921924}]}'''
        self.assertEqual(json, expected)

    def test_nesting_aliases(self):
        kson = '''
        {
  "comments": "This is a KSON document containing a list of chess grandmasters.",
  "grandmasterList": <<!https://api.chess.com/pub/titled/GM>> as gms,
  /* This API returns in the format { players: [ ... ] }, so we need to dereference one level into the object: */
  "gmListAscending": ```select players from "$gms$players" order by players asc limit 10
   ```
        }
        '''
        json = KSONCompiler().run(kson)

        expected = '''{"comments": "This is a KSON document containing a list of chess grandmasters.", "grandmasterList": "https://api.chess.com/pub/titled/GM", "gmListAscending": ["123lt", "124chess", "1977ivan", "1stsecond", "4thd-alpeacefulmoon", "64aramis64", "64arthos64", "64atilla64", "64dartagnan64", "64genghis64"]}'''
        self.assertEqual(json, expected)


if __name__ == '__main__':
    unittest.main()
