import unittest

from ksonpy.kson_parser import KSONParser


def is_equivalent_to_json(obj, raw=False):
    import json
    s = obj if raw else json.dumps(obj)
    p = KSONParser().decode(s)

    is_correct = json.dumps(p) == json.dumps(json.loads(s))
    if not is_correct:
        print(s)
        print(p)
        raise ValueError(f"Not equivalent to JSON: {obj}")
    print(f'Parsed: {s} := {p}')


class TestParser(unittest.TestCase):

    def test_numbers(self):
        is_equivalent_to_json('0', True)
        is_equivalent_to_json('42.000', True)
        is_equivalent_to_json('1.1', True)
        is_equivalent_to_json('1.00e2', True)
        is_equivalent_to_json('0.01E-5', True)
        is_equivalent_to_json('5E15', True)
        is_equivalent_to_json('0e-04', True)

    def test_strings(self):
        is_equivalent_to_json('')
        is_equivalent_to_json('Some characters')
        is_equivalent_to_json('\"You can quote me\" \t yes, sir')
        is_equivalent_to_json('New \n lines, new \n places')
        is_equivalent_to_json('Unicodez: \u1002, \u56ff, \u0356')

    def test_arrays(self):
        is_equivalent_to_json([])
        is_equivalent_to_json(' [ 0, 4  , 3.23e5    ]', True)
        is_equivalent_to_json([[[]]])
        is_equivalent_to_json([3, "a string", ["nested array", 42.0]])

    def test_dicts(self):
        is_equivalent_to_json({})
        is_equivalent_to_json('  { "key" : "you see how it is", "another key": 42.0   }', True)
        is_equivalent_to_json(
            {'a': 0.5, 'b': "String \u2223", 'c': ['Array', 'of', 'things'], 'd': {'Another': 'dict'}})

    def test_keywords(self):
        is_equivalent_to_json('true', True)
        is_equivalent_to_json('false', True)
        is_equivalent_to_json('null', True)
        is_equivalent_to_json([True, False, None, [True, True, None, False]])

    def test_refs(self):
        out = KSONParser().decode('''
        {
            "comments": "This is a KSON document containing a reference.",
            "ref": <<https://api.chess.com/pub/titled/GM|json>>
        
        }
        ''')

        print(str(out))

    def test_comments(self):
        out = KSONParser().decode('''[
            1, 2,
            /* Just to note: We're about to hit the 3rd element of the array! Watch out! */
            3
            
        ]''')

        print(out)

    def test_aliases(self):
        out = KSONParser().decode('''
        {
           "first": <<https://mlb.com>> as baseball,
           "second": [
              0 as _num,
              1, 2, 3
           ] as nums
        }
        ''')

        print(out)

    def test_sql(self):
        out = KSONParser().decode('''
        {
           "first": <<https://mlb.com>> as baseball,
           "second": [
              0 as _num,
              1, 2, 3
           ] as nums
        }
        ''')


if __name__ == '__main__':
    unittest.main()
